import m from "mithril";
//import { getKeys, getNS, getClosestComponent } from '../../utils/utils'
//require('../../utils/utils')
require('dom4')

import 'materialize-css/dist/css/materialize.min.css';
import 'material-icons/iconfont/material-icons.css';

import { TextField, RaisedButton, Button } from 'polythene-mithril'
import { addTypography } from "polythene-css"
import "polythene-css"

import { ButtonCSS } from "polythene-css"
import 'mdbootstrap/css/bootstrap.min.css';
import 'mdbootstrap/css/mdb.min.css';
//import 'fontawesome';
import PopperJs from 'mdbootstrap/js/popper.min.js';

//import Actions from './user_actions'
import {State} from './user_state'
import {Form1, Form2, Form3, Form4, Form5} from './user_form'

// import AOS from 'aos';
// import 'aos/dist/aos.css'; // You can also use <link> for styles
// import { ioxhost } from "fontawesome";

ButtonCSS.addStyle(".bordered-button", {
  color_light_text:   "#673ab7",
  color_light_border: "#673ab7",
  color_dark_text:    "yellow",
  color_dark_border:  "yellow"
})  

ButtonCSS.addStyle(".submit-button", {
  color_light_text:       "#fff",
  color_light_background: "#90EE90",
})


const BreadCrumbs = {
  // this just adapts to the form
  oncreate: (vnode) => {
    //AOS.init()
    //var component = vnode.dom.closest('[tab-series]');
    //var elem_array = Array.from(component.getElementsByTagName('li'))
    //var tab_series = Number(component.getAttribute('tab-series'))
    //elem_array.map((elem, i, arr) => {
    //  elem.tabIndex = tab_series+i
    //})
    //elem_array[0].focus()
  },
  view: ({attrs:{state,actions}}) => {
    var _state = actions.getStateInContext(actions);
    var step_keys = Object.keys(state.formState.forms)
    var current_step = m.route.param('stepName')
    return [
      m("nav", {"aria-label":"breadcrumb"}, 
        m("ol", {"class":"breadcrumb peach-gradient"}, step_keys.map((step, i, arr) => 
          m("li", {"class":`breadcrumb-item${ step === current_step ? ' active' : '' }`}, [
              m("a", {"class":"h5-responsive","href":`${m.route.prefix + m.route.get().replace(m.route.param('stepName'), step)}`}, 
                step
              )
            ]
          )
        ))
      )
    ]
  }
}


const PrevNextSubmit = {
  oncreate: (vnode) => {
    //var component = vnode.dom.closest('[tab-series]');
    //var tab_series = Number(component.getAttribute('tab-series'))
    //Array.from(component.getElementsByTagName('a')).map((elem, i, arr) => {
    //  elem.tabIndex = tab_series+i
    //})
  },
  view: ({attrs:{state,actions}}) => {
    var _state = actions.getStateInContext(actions);
    var step_keys = Object.keys(state.formState.forms)
    var current_step_name = m.route.param('stepName')
    var current_step_indx = step_keys.indexOf(current_step_name)
    var next_step_indx = current_step_indx + FormDirection
    var next_step_name = step_keys[next_step_indx]
    var next_form = state.formState.forms[next_step_name]


    const btn_config = [
      {
        name:"prev",   
        disabled:true, 
        events:{}, 
        label:"Prev"
      },
      {
        name:"next",   
        disabled:true, 
        events:{}, 
        label:"Next"
      },
      {
        name:"submit", 
        disabled:true, 
        events:{},
        label:"Submit"
      }
    ]
    var prev_btn_config = btn_config[0]
    var next_btn_config = btn_config[1]
    var submit_btn_config = btn_config[2]
    var current_step_indx, steps
    //// 
    steps = state.formState.forms
    // prev button state
    if(current_step_indx > 0){
      prev_btn_config['disabled'] = false
      prev_btn_config['events']['onclick'] = (e) => {
        var next_step_name = step_keys[current_step_indx - 1]
        var cur_route = m.route.get()
        var new_route = cur_route.replace(m.route.param('stepName'), next_step_name)
        m.route.set(new_route)
      }
    }
    // next button state
    if(current_step_indx >= 0 && current_step_indx <= step_keys.length - 1){
      next_btn_config['disabled'] = false
      next_btn_config['events']['onclick'] = (e) => {
        var next_step_name = step_keys[current_step_indx + 1]
        var cur_route = m.route.get()
        var new_route = cur_route.replace(m.route.param('stepName'), next_step_name)
        m.route.set(new_route)
      }
    }
    // submit button state
    if(current_step_indx == step_keys.length - 1){
      next_btn_config['disabled'] = true
      submit_btn_config['disabled'] = false
      submit_btn_config['className'] = 'submit-button'
      submit_btn_config['events']['onclick'] = (e) => {
        console.log("CLICK", state.actions.getFormState())
      }
    }
    return btn_config.map((item, i, arr) => m(Button, btn_config[i]))
  }
}

// -------------------------------------------------------------------------//
// -------------------------------------------------------------------------//
// -------------------------------------------------------------------------//


// -------------------------------------------------------------------------//
// -------------------------------------------------------------------------//
// -------------------------------------------------------------------------//

var processEvents = (e, state) => {
  // var title = `///---->  ${e.type}  <----///`
  // console.log(title)
  // console.log(e.target)
  if(e.type==="onsubmit"){
    console.log(e)
  }

  // ----------------------------- //
  // --------> ONKEYDOWN <-------- //
  // ----------------------------- //
  if(e.type==="keydown"){
    if ((e.key === "Tab" || e.keyCode === 13) && !e.shiftKey) {
      if(e.target.hasAttribute('last')){
        e.preventDefault();
        FormDirection = 1;
        state.actions.go();
      }
    }
    if (e.key === "Tab" && e.shiftKey) {
      if(e.target.hasAttribute('first')){
        FormDirection = -1;
        state.actions.go();
      }
    }
  };
  // ----------------------------- //
  // ----------> ONBLUR <--------- //
  // ----------------------------- //
  //if(e.type==="blur"){};
  // ----------------------------- //
  // ---------> ONFOCUS <--------- //
  // ----------------------------- //
  //if(e.type==="focus"){};
}

const state = State()

const Actions = (state) => ({
  procEvent: (e) => {
    processEvents(e, state)
  },
  getClinicComponent: (vnode) => {
    return vnode.dom.closest('[component=ClinicComponent]')
  },
  getPath: () => {
    let prefix = m.route.param('crudOp')
    let suffix = m.route.param('onType')
    return `${prefix}${suffix}`
  },
  getStateInContext: (actions) => {
    return state.cntx[actions.getPath()]
  },
  getPathConfig: (actions) => {
    return state.cntx[actions.getPath()]
  },
  getContextSteps: (actions) => {
    var config = actions.getPathConfig(actions)
    return config.steps
  },
  getFormState: () => {
    var form_name = state.actions.getPath().toLowerCase()
    return state[form_name]
  },
  getStepState: () => {
    var form_name = state.actions.getPath().toLowerCase()
    var step_name = m.route.param('stepName')
    if(!state[form_name]){
      state[form_name] = {}
    }
    if(!state[form_name][step_name]){
      state[form_name][step_name] = {}
    }
    return state[form_name][step_name]
  },
  withFocus: (formState) => ({
    onChange: (obj) => {
      state.actions.onChange(obj);
      if (!state.formState.focusSet) {
        obj.el.focus();
        state.formState.focusSet = true;
      }
    }
  }),
  onChange: (obj) => {
    var form_name = state.actions.getPath().toLowerCase()
    var field_name = obj.el.getAttribute('name')
    var step_name = m.route.param('stepName')

    if(obj.value){
      if(!state[form_name]){
        state[form_name] = {}
      }
      if(!state[form_name][step_name]){
        state[form_name][step_name] = {}
      }
      if(!state[form_name][step_name][field_name]){
        state[form_name][step_name][field_name] = ""
      }
      state[form_name][step_name][field_name] = obj.value
    }    
  },
  firstFieldFocus: () => {
    return (FormDirection === state.DIRECTIONS.FORWARD 
      ? state.actions.withFocus(state.formState) 
      : undefined
  )},
  lastFieldFocus: () => {
    return (FormDirection === state.DIRECTIONS.BACKWARD 
      ? state.actions.withFocus(state.formState) 
      : undefined
  )},
  go: () => {
    var step_keys = Object.keys(state.formState.forms)
    var step_name = m.route.param('stepName')
    var current_step_indx = step_keys.indexOf(step_name)
    var next_step_indx = current_step_indx + FormDirection
    var next_step_name = step_keys[next_step_indx]
    var next_form = state.formState.forms[next_step_name]
    if(next_form){
      m.route.set(m.route.get().replace(step_name, next_step_name))
    }
  }
})

let FormDirection = 1
const actions = Actions(state)


const FormComponent = {
  view: ({attrs:{state,actions}}) => {
    state.actions = actions
    state.DIRECTIONS = {
      FORWARD: 1,
      BACKWARD: -1
    };
    state.formState = {
      focusSet: false,
      forms: [],
    };
    state.formState.forms = {
      details:Form1,
      address:Form2,
      suggest:Form3,
      more_info:Form4,
      addon:Form5
    };
    state.events = {
      onfocus:actions.procEvent,
      onblur:actions.procEvent,
      oninput:actions.procEvent,
      onclick:actions.procEvent,
      onkeydown:actions.procEvent
    };
    //var _state = state.actions.getStateInContext(state.actions);
    var step_name = m.route.param('stepName');
    let Form = {view: ({attrs:{state,actions}}) => {return m('div', {}, "NOT FOUND")}}
    if(Object.keys(state.formState.forms).includes(step_name)){
      Form = state.formState.forms[step_name];
      state.formState.currentFormName = step_name;
      state.form_api_verion = '0.0.1';
    }
    console.log('state',state.formState.forms)
    return [
      m("div", {"style":"margin-top: 0vh", "tab-series":1000, "component":"BreadCrumbs"},     m(BreadCrumbs,    {state,actions})),
      m('div', {"style":"margin-top: 3vh", "tab-series":2000, "component":"DynamicForm", 
        "form_name":"example", "form_version":"0.0.1"},                                       m(Form,           {state,actions})),
      m("div", {"style":"margin-top: 6vh", "tab-series":3000, "component":"PrevNextSubmit"},  m(PrevNextSubmit, {state,actions}))
    ]
  }
};

var App = {
  view: function() {
    return m(FormComponent, {state, actions})
  }
}
m.mount(document.getElementById("root"), App);

///

//onblur 	        Fires the moment that the element loses focus
//onchange 	      Fires the moment when the value of the element is changed
//oncontextmenu   Script to be run when a context menu is triggered
//onfocus 	      Fires the moment when the element gets focus
//oninput 	      Script to be run when an element gets user input
//oninvalid 	    Script to be run when an element is invalid
//onreset 	      Fires when the Reset button in a form is clicked
//onsearch 	      Fires when the user writes something in a search field (for <input="search">)
//onselect 	      Fires after some text has been selected in an element
//onsubmit 	      Fires when a form is submitted

//onkeydown 	    Fires when a user is pressing a key
//onkeypress 	    Fires when a user presses a key
//onkeyup 	      Fires when a user releases a key

///
