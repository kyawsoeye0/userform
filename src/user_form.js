import m from "mithril";
import { TextField } from "polythene-mithril"
import stream from "mithril/stream"

//import { getElemsByAttrVal } from '../../utils/utils'

const Form1 = {
  oninit: (vnode) => {
    // Reset focusSet state
    vnode.attrs.state.formState.focusSet = false;
  },
  view: ({attrs:{state,actions}}) => {
    var step_state = state.actions.getStepState()
    return [
      m("h4", "Your details"),
      m(TextField, {
        label: "Email",
        type: "email",
        onChange: actions.onChange,
        value: (step_state['email'] == undefined) ? "" : step_state['email'],
        domAttributes: {"first":"TextField", "name":"email"},
        ...actions.firstFieldFocus(),
        events: state.events
      }),
      m(TextField, {
        label: "First Name",
        type: "text",
        value: (step_state['first_name'] == undefined) ? "" : step_state['first_name'],
        domAttributes: {"name":"first_name"},
        onChange: actions.onChange,
        events: state.events
      }),
      m(TextField, {
        label: "Last Name",
        type: "text",
        domAttributes: {"last":"TextField", "name":"last_name"},
        value: (step_state['last_name'] == undefined) ? "" : step_state['last_name'],
        onChange: actions.onChange,
        ...actions.lastFieldFocus(),
        events: state.events
      })
    ]
  }
};



const Form2 = {
  oninit: (vnode) => {
    // Reset focusSet state
    vnode.attrs.state.formState.focusSet = false;
  },
  view: ({attrs:{state,actions}}) => {
    var step_state = state.actions.getStepState()
    return [
      m("h4", "Your address"),
      m(TextField, {
        label: "Street number",
        type: "number",
        onChange: actions.onChange,
        value: (step_state['street_number'] == undefined) ? "" : step_state['street_number'],
        domAttributes: {"first":"TextField", "name":"street_number"},
        ...actions.firstFieldFocus(),
        events: state.events
      }),
      m(TextField, {
        label: "Street Name",
        type: "text",
        value: (step_state['street_name'] == undefined) ? "" : step_state['street_name'],
        domAttributes: {"name":"street_name"},
        onChange: actions.onChange,
        events: state.events
      }),
      m(TextField, {
        label: "City",
        type: "text",
        value: (step_state['city'] == undefined) ? "" : step_state['city'],
        domAttributes: {"name":"city"},
        onChange: actions.onChange,
        events: state.events
      }),
      m(TextField, {
        label: "State",
        type: "text",
        value: (step_state['state'] == undefined) ? "" : step_state['state'],
        domAttributes: {"name":"state"},
        onChange: actions.onChange,
        events: state.events
      }),

      m(TextField, {
        label: "Zipcode",
        type: "text",
        domAttributes: {"last":"TextField", "name":"zip"},
        value: (step_state['zip'] == undefined) ? "" : step_state['zip'],
        onChange: actions.onChange,
        ...actions.lastFieldFocus(),
        events: state.events
      })
    ]
  }
};

const Form3 = {
  oninit: (vnode) => {
    // Reset focusSet state
    vnode.attrs.state.formState.focusSet = false;
  },
  view: ({attrs:{state,actions}}) => {
    var step_state = state.actions.getStepState()
    return [
      m("h4", "Suggest a friend"),
      m(TextField, {
        label: "Email",
        type: "email",
        onChange: actions.onChange,
        value: (step_state['email'] == undefined) ? "" : step_state['email'],
        domAttributes: {"first":"TextField", "name":"email"},
        ...actions.firstFieldFocus(),
        events: state.events
      }),
      m(TextField, {
        label: "First Name",
        type: "text",
        value: (step_state['first_name'] == undefined) ? "" : step_state['first_name'],
        domAttributes: {"name":"first_name"},
        onChange: actions.onChange,
        events: state.events
      }),
      m(TextField, {
        label: "Last Name",
        type: "text",
        domAttributes: {"last":"TextField", "name":"last_name"},
        value: (step_state['last_name'] == undefined) ? "" : step_state['last_name'],
        onChange: actions.onChange,
        ...actions.lastFieldFocus(),
        events: state.events
      })
    ]
  }
};

const Form4 = {
  oninit: (vnode) => {
    // Reset focusSet state
    vnode.attrs.state.formState.focusSet = false;
  },
  view: ({attrs:{state,actions}}) => {
    var step_state = state.actions.getStepState()
    return [
      m("h4", "Details matter"),
      m(TextField, {
        label: "More info 1",
        type: "text",
        onChange: actions.onChange,
        value: (step_state['more_info_1'] == undefined) ? "" : step_state['more_info_1'],
        domAttributes: {"first":"TextField", "name":"more_info_1"},
        ...actions.firstFieldFocus(),
        events: state.events
      }),
      m(TextField, {
        label: "More info 2",
        type: "text",
        value: (step_state['more_info_2'] == undefined) ? "" : step_state['more_info_2'],
        domAttributes: {"name":"more_info_2"},
        onChange: actions.onChange,
        events: state.events
      }),
      m(TextField, {
        label: "More info 3",
        type: "text",
        value: (step_state['more_info_3'] == undefined) ? "" : step_state['more_info_3'],
        domAttributes: {"name":"more_info_3"},
        onChange: actions.onChange,
        events: state.events
      }),
      m(TextField, {
        label: "Last of the more info",
        type: "text",
        domAttributes: {"last":"TextField", "name":"more_info_4"},
        value: (step_state['more_info_4'] == undefined) ? "" : step_state['more_info_4'],
        onChange: actions.onChange,
        ...actions.lastFieldFocus(),
        events: state.events
      })
    ]
  }
};


const Form5 = {
  oninit: (vnode) => {
    // Reset focusSet state
    vnode.attrs.state.formState.focusSet = false;
  },
  view: ({attrs:{state,actions}}) => {
    var step_state = state.actions.getStepState()
    return [
      m("h4", "More details"),
      m(TextField, {
        label: "More info 1",
        type: "text",
        onChange: actions.onChange,
        value: (step_state['more_info_1'] == undefined) ? "" : step_state['more_info_1'],
        domAttributes: {"first":"TextField", "name":"more_info_1"},
        ...actions.firstFieldFocus(),
        events: state.events
      }),
      m(TextField, {
        label: "Anything more to offer",
        type: "text",
        value: (step_state['more_info_2'] == undefined) ? "" : step_state['more_info_2'],
        domAttributes: {"name":"more_info_2"},
        onChange: actions.onChange,
        events: state.events
      }),
      m(TextField, {
        label: "More is always better",
        type: "text",
        value: (step_state['more_info_3'] == undefined) ? "" : step_state['more_info_3'],
        domAttributes: {"name":"more_info_3"},
        onChange: actions.onChange,
        events: state.events
      }),
      m(TextField, {
        label: "Why are americans are so...",
        type: "text",
        domAttributes: {"last":"TextField", "name":"more_info_4"},
        value: (step_state['more_info_4'] == undefined) ? "" : step_state['more_info_4'],
        onChange: actions.onChange,
        ...actions.lastFieldFocus(),
        events: state.events
      })
    ]
  }
};


module.exports = {
  Form1,
  Form2,
  Form3,
  Form4,
  Form5
}